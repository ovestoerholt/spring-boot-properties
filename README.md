# README #

spring-boot-properties is an example to show how to read application configuration from application.properties or application-{environment}.properties.


### What I have learned ###

* The file application.properties is used by default.
* If you specify the environment variable SPRING_BOOT_ACTIVE=test then application-test.properties is used instead.
* Values not specified in application-test.properties are NOT inherited from (the 'default') application.properties.